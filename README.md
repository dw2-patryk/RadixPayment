﻿# Boas vendas!
## Utilizar a ultima versão do Visual Studio 2017
## Atualizar o .Net Core 2 (última versão)
## Abrir a solução (Radix.Payment.sln)
## Compilar a aplicação (F6)
## Verificar as permissões necessárias para o banco (LocalDb que está em src\00 - Presentation\App_Data)
## Executar (F5) ou realizar o deploy após compilar a aplicação!
###### Chamar a action: (HttpPut) /api/v1.0/payment/{id}
###### no body, informar o json equivalente ao modelo canônico que está em  src\01 - Domain\01 - DTO\Radix.Payment.Domain.DTO\OrderRequest.cs
###### ------
###### Chamar a action: (HttpGet) /api/v1.0/transaction/{id} para ver as transações de um lojista...