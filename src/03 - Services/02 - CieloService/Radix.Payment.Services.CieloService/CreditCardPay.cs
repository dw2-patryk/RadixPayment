﻿using RestSharp;

namespace Radix.Payment.Services.CieloService
{
    public class CreditCardPay
    {
        public string EndPoint { get; set; }

        public Domain.Models.Cielo.Response.CreditCardResponse Pay(Domain.Models.Cielo.Request.CreditCardRequest _requestParam)
        {
            var client = new RestClient(this.EndPoint);
            var request = new RestRequest("/Sale/", Method.POST);
            request.AddJsonBody(_requestParam);

            return client.Execute<Domain.Models.Cielo.Response.CreditCardResponse>(request).Data;
        }
    }
}
