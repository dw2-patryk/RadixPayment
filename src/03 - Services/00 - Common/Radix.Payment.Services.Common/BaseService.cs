﻿using RestSharp;
using System;

namespace Radix.Payment.Services.Common
{
    public abstract class BaseService<T> 
    {
        public string Url { get; set; }
    }
}
