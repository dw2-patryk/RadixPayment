﻿using RestSharp;
using System;

namespace Radix.Payment.Services.ClearSaleService
{

    public class ClearValidate
    {
        public string EndPoint { get; set; }
        public Domain.Models.ClearSale.Response.ClearLogin clearLogin { get; set; } = null;

        public bool Login(Domain.Models.ClearSale.Request.ClearLogin _requestParam)
        {
            try
            {
                if (clearLogin == null)
                {
                    var client = new RestClient(this.EndPoint);
                    var request = new RestRequest("auth/login", Method.POST);

                    request.AddJsonBody(_requestParam);
                    clearLogin = client.Execute<Domain.Models.ClearSale.Response.ClearLogin>(request).Data;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public Domain.Models.ClearSale.Response.ClearValidate Validate(Domain.Models.ClearSale.Request.ClearValidate _requestParam)
        {
            var client = new RestClient(this.EndPoint);
            var request = new RestRequest("order/send", Method.POST);
            request.AddJsonBody(_requestParam);

            return client.Execute<Domain.Models.ClearSale.Response.ClearValidate>(request).Data;
        }
    }
}
