﻿using RestSharp;

namespace Radix.Payment.Services.StoneService
{
    public class CreditCardPay
    {
        public string EndPoint { get; set; }

        public Domain.Models.Stone.Response.CreditCardResponse Pay(Domain.Models.Stone.Request.CreditCardRequest _requestParam)
        {
            var client = new RestClient(this.EndPoint);
            var request = new RestRequest("/1/sales/", Method.POST);
            request.AddJsonBody(_requestParam);

            return client.Execute<Domain.Models.Stone.Response.CreditCardResponse>(request).Data;
        }
    }
}
