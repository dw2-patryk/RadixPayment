﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Radix.Payment.API.Common
{
    public enum CommonErrors
    {
        PAY_ERROR = 0
    }

    public class CommonResponse
    {
        public dynamic Value { get; set; }
        public CommonResponseError Error { get; set; }
    }

    public class CommonResponseError
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Target { get; set; }
        public Exception Details { get; set; }
        public string InnerError { get; set; }
    }
}
