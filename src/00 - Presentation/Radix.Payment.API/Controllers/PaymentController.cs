﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Radix.Payment.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class PaymentController : BaseController
    {
        // GET api/payment
        [HttpGet]
        public dynamic Get()
        {
            var bus = new Domain.Business.Lojista();
            var x = bus.Listar(); 

            return new { a = "value1" };
        }

        // GET api/payment/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/payment
        [HttpPut("{id}")]
        public Common.CommonResponse Pay(long id, [FromBody]string value)
        {
            var res = new Common.CommonResponse();
            var bus = new Domain.Business.Pay(id);

            try
            {
                res.Value = bus.doPayment(value);
            }
            catch (Exception e)
            {
                res.Error = new Common.CommonResponseError()
                {
                    Code = Common.CommonErrors.PAY_ERROR.ToString(),
                    Message = "Erro ao realizar pagamento!",
                    Target = "Domain.Business.Pay.doPayment()",
                    Details = e.InnerException,
                    InnerError = e.StackTrace

                };
            }

            return res;
        }

        // POST api/payment
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/payment/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/payment/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
