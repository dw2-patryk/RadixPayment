﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Radix.Payment.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class TransactionController : BaseController
    {
        // GET api/transaction
        [HttpGet]
        public dynamic Get()
        {
            var bus = new Domain.Business.Lojista();
            var x = bus.Listar(); 

            return new { a = "value1" };
        }

        // GET api/transaction/5
        [HttpGet("{id}")]
        public Common.CommonResponse Get(int id)
        {
            var bus = new Domain.Business.Lojista();
            var _lojista = bus.GetItem(id);

            return new Common.CommonResponse()
            {
                Value = _lojista.transacoes
            };
        }
    }
}
