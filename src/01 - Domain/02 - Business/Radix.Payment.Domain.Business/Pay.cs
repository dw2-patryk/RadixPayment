﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Radix.Payment.Domain.Business
{
    public class Pay
    {
        private long LojistaId { get; set; }
        public bool Testing { get; set; } = false;
        private Services.ClearSaleService.ClearValidate Validator { get; set; } = new Services.ClearSaleService.ClearValidate();

        public Pay(long _lojistaId)
        {
            this.LojistaId = _lojistaId;
        }

        public string doPayment(string jsonBody)
        {
            if (Testing)
            {
                return "OK";
            }

            try
            {
                var busLojista = new Lojista();
                var busTransacao = new Transacao();
                var lojista = busLojista.GetItem(this.LojistaId);
                var reqDTO = JToken.Parse(jsonBody).ToObject<DTO.OrderRequest>();
                var bCompra = true;

                if (reqDTO.OrderType != "CreditCard")
                {
                    throw new Exception("Só é aceita a opção cartão de crédito.");
                }

                // validacao antifraude
                if (lojista.antifraudes.Any())
                {
                    if (Validate(lojista))
                    {
                        var _validator = lojista.antifraudes.FirstOrDefault();
                        var _validateObject = new Models.ClearSale.Request.ClearValidate()
                        {
                            AnalysisLocation = "BRA",
                            ApiKey = _validator.ApiKey,
                            LoginToken = this.Validator.clearLogin.Token.Value,
                            Reanalysis = false,
                            Orders = new List<Models.ClearSale.Request.Order>()
                            {
                                new Models.ClearSale.Request.Order()
                                {
                                    ID = reqDTO.ID,
                                    Date = DateTime.Now,
                                    Email = reqDTO.Email,
                                    TotalItems = reqDTO.TotalItems,
                                    TotalOrder = reqDTO.TotalOrder,
                                    TotalShipping = reqDTO.TotalShipping,
                                    IP = reqDTO.IP,
                                    Currency = reqDTO.Currency,

                                    Payments = new List<Models.ClearSale.Request.Payment>()
                                    {
                                        new Models.ClearSale.Request.Payment()
                                        {
                                            Date = DateTime.Now.ToString("dd/MM/yyyy H:mm:ss"),
                                            Type = 2,
                                            CardNumber = reqDTO.CardNumber,
                                            CardHolderName = reqDTO.CardHolderName,
                                            CardExpirationDate = reqDTO.CardExpirationDate,
                                            Amount = reqDTO.Amount,
                                            PaymentTypeID = 1,
                                            CardType = 1,
                                            CardBin = reqDTO.CardBin
                                        }
                                    },

                                    BillingData = new Models.ClearSale.Request.BillingData()
                                    {
                                        Address = new Models.ClearSale.Request.Address()
                                        {
                                            Street = reqDTO.bill.client.Street,
                                            City = reqDTO.bill.client.City,
                                            State = reqDTO.bill.client.State,
                                            Comp = reqDTO.bill.client.Comp,
                                            ZipCode = reqDTO.bill.client.ZipCode,
                                            County = reqDTO.bill.client.County,
                                            Number = reqDTO.bill.client.Number
                                        },

                                        BirthDate = reqDTO.bill.client.BirthDate,
                                        Gender = reqDTO.bill.client.Gender,
                                        Name = reqDTO.bill.client.Name,
                                        Email = reqDTO.bill.client.Email,

                                        Phones = reqDTO.bill.phones.Cast<Models.ClearSale.Request.Phone>().ToList()

                                    },

                                    ShippingData = new Models.ClearSale.Request.ShippingData()
                                    {
                                        Address = new Models.ClearSale.Request.Address()
                                        {
                                            Street = reqDTO.shipping.client.Street,
                                            City = reqDTO.shipping.client.City,
                                            State = reqDTO.shipping.client.State,
                                            Comp = reqDTO.shipping.client.Comp,
                                            ZipCode = reqDTO.shipping.client.ZipCode,
                                            County = reqDTO.shipping.client.County,
                                            Number = reqDTO.shipping.client.Number
                                        },

                                        BirthDate = reqDTO.shipping.client.BirthDate,
                                        Gender = reqDTO.shipping.client.Gender,
                                        Name = reqDTO.shipping.client.Name,
                                        Email = reqDTO.shipping.client.Email,

                                        Phones = reqDTO.shipping.phones.Cast<Models.ClearSale.Request.Phone>().ToList()
                                    },

                                    Items = reqDTO.products.Cast<Models.ClearSale.Request.Item>().ToList(),
                                    SessionID = ""
                                }
                            }
                        };

                        var ret = this.Validator.Validate(_validateObject);

                        if (ret.Orders.Any() && ret.Orders.FirstOrDefault().Status != "APA")
                        {
                            bCompra = false;
                        }
                    }
                }

                // fim validacao

                // inicio compra
                if (bCompra)
                {
                    if (lojista.adquirentes.Any())
                    {
                        ProcessPayment(lojista, reqDTO);
                    }
                }
                else
                {
                    throw new Exception("Compra com suspeita de fraude!");
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return "OK";
        }

        public bool ProcessPayment(Models.API.Lojista lojista, DTO.OrderRequest order)
        {
            if (lojista.adquirentes.Count() > 1)
            {
                if (order.CardBrand == "VISA")
                {
                    return ProcessCielo(lojista, order);
                }
                else
                {
                    return ProcessStone(lojista, order);
                }
            }
            else if (lojista.adquirentes.Count() > 0)
            {
                if (lojista.adquirentes.FirstOrDefault().adquirente.AdquirenteId == (int)DTO.EnumAdquirente.Stone)
                {
                    return ProcessStone(lojista, order);
                } else
                {
                    return ProcessCielo(lojista, order);
                }
            }

            return true;
        }

        public bool ProcessCielo(Models.API.Lojista lojista, DTO.OrderRequest order)
        {
            var busTransacao = new Transacao();
            var _cielo = new Services.CieloService.CreditCardPay();
            _cielo.EndPoint = lojista.adquirentes.First(x => x.adquirente.AdquirenteId == (int)DTO.EnumAdquirente.Cielo).adquirente.EndPoint;

            _cielo.Pay(new Models.Cielo.Request.CreditCardRequest()
            {
                Customer = new Models.Cielo.Request.Customer() {
                    Name = order.bill.client.Name
                },
                MerchantOrderId = long.Parse(order.OrderReference),
                Payment = new Models.Cielo.Request.Payment()
                {
                    Amount = order.Amount,
                    CreditCard = new Models.Cielo.Request.CreditCard()
                    {
                        Brand = order.CardBrand,
                        CardNumber = order.CardNumber,
                        ExpirationDate = order.CardExpirationDate,
                        Holder = order.CardHolderName,
                        SecurityCode = long.Parse(order.CardBin)
                    },
                    Installments = order.Installments,
                    SoftDescriptor = order.bill.client.Name, // não sei!
                    Type = order.OrderType
                }
            });

            busTransacao.Save(new Models.API.Transacao()
            {
                lojista = new Models.API.Lojista() { LojistaId = this.LojistaId },
                JsonTransacao = JValue.FromObject(new
                {
                    Log = "Lojista " + lojista.Nome + " realizou uma compra coma Cielo em " + DateTime.Now.ToString("dd/MM/yyyy H:mm:ss") + "."
                }).ToString(),
                DataCadastro = DateTime.Now
            });

            return true;
        }

        public bool ProcessStone(Models.API.Lojista lojista, DTO.OrderRequest order)
        {
            var busTransacao = new Transacao();
            var _stone = new Services.StoneService.CreditCardPay();
            _stone.EndPoint = lojista.adquirentes.First(x => x.adquirente.AdquirenteId == (int)DTO.EnumAdquirente.Stone).adquirente.EndPoint;

            _stone.Pay(new Models.Stone.Request.CreditCardRequest() {
                CreditCardTransactionCollection = new List<Models.Stone.Request.CreditCardTransactionCollection>()
                {
                    new Models.Stone.Request.CreditCardTransactionCollection()
                    {
                        AmountInCents = order.Amount,
                        CreditCard = new Models.Stone.Request.CreditCard()
                        {
                            CreditCardBrand = order.CardBrand,
                            CreditCardNumber = order.CardNumber,
                            ExpMonth = int.Parse(order.CardExpirationDate.Split('/')[0].ToString()),
                            ExpYear = int.Parse(order.CardExpirationDate.Split('/')[1].ToString()),
                            HolderName = order.CardHolderName,
                            SecurityCode = long.Parse(order.CardBin)
                        },
                        InstallmentCount = order.Installments
                    }                    
                },
                Order = new Models.Stone.Request.Order()
                {
                    OrderReference = order.OrderReference
                }
            });

            busTransacao.Save(new Models.API.Transacao()
            {
                lojista = new Models.API.Lojista() { LojistaId = this.LojistaId },
                JsonTransacao = JValue.FromObject(new
                {
                    Log = "Lojista " + lojista.Nome + " realizou uma compra coma Cielo em " + DateTime.Now.ToString("dd/MM/yyyy H:mm:ss") + "."
                }).ToString(),
                DataCadastro = DateTime.Now
            });
            return true;
        }

        public bool Validate(Models.API.Lojista lojista)
        {
            var busTransacao = new Transacao();
            var _validator = lojista.antifraudes.FirstOrDefault();

            if (this.Validator.Login(new Models.ClearSale.Request.ClearLogin()
            {
                Login = new Models.ClearSale.Request.Login()
                {
                    Apikey = _validator.ApiKey,
                    ClientId = _validator.ClientId,
                    ClientSecret = _validator.ClientSecret
                }
            }))
            {
                busTransacao.Save(new Models.API.Transacao()
                {
                    lojista = new Models.API.Lojista() { LojistaId = this.LojistaId },
                    JsonTransacao = JValue.FromObject(new
                    {
                        Log = "Lojista " + lojista.Nome + " realizou validação no " + _validator.antifraude.Nome + " em " + DateTime.Now.ToString("dd/MM/yyyy H:mm:ss") + "."
                    }).ToString(),
                    DataCadastro = DateTime.Now
                });

                return true;
            }

            return false;
        }
    }
}
