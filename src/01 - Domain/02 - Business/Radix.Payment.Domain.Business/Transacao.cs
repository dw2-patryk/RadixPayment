﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Business
{
    public class Transacao
    {
        public List<Models.API.Transacao> Listar()
        {
            var repo = new Infra.Data.Transacao();
            return repo.Listar();
        }

        public Models.API.Transacao GetItem(long _id)
        {
            var repo = new Infra.Data.Transacao();
            return repo.GetItem(_id);
        }

        public bool Save(Models.API.Transacao _item)
        {
            var repo = new Infra.Data.Transacao();
            return repo.Save(_item);
        }

        public bool Delete(Models.API.Transacao _item)
        {
            var repo = new Infra.Data.Transacao();
            return repo.Delete(_item);
        }
    }
}
