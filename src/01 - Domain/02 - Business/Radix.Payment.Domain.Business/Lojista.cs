﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Business
{
    public class Lojista
    {
        public List<Models.API.Lojista> Listar()
        {
            var repo = new Infra.Data.Lojista();
            return repo.Listar();
        }

        public Models.API.Lojista GetItem(long _id)
        {
            var repo = new Infra.Data.Lojista();
            return repo.GetItem(_id);
        }

        public bool Save(ref Models.API.Lojista _item)
        {
            var repo = new Infra.Data.Lojista();
            return repo.Save(ref _item);
        }

        public bool Delete(Models.API.Lojista _item)
        {
            var repo = new Infra.Data.Lojista();
            return repo.Delete(_item);
        }
    }
}
