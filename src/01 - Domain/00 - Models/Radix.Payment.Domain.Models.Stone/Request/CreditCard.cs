﻿using System;

namespace Radix.Payment.Domain.Models.Stone.Request
{
    public partial class CreditCard
    {
        public string CreditCardBrand { get; set; }
        public string CreditCardNumber { get; set; }
        public long ExpMonth { get; set; }
        public long ExpYear { get; set; }
        public string HolderName { get; set; }
        public long SecurityCode { get; set; }
    }
}
