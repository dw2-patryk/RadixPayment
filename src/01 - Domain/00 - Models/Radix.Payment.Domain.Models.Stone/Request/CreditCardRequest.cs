﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Models.Stone.Request
{
    public partial class CreditCardRequest
    {
        public List<CreditCardTransactionCollection> CreditCardTransactionCollection { get; set; }
        public Order Order { get; set; }
    }

}
