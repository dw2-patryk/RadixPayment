﻿using System;

namespace Radix.Payment.Domain.Models.Stone.Request
{

    public partial class CreditCardTransactionCollection
    {
        public long AmountInCents { get; set; }
        public CreditCard CreditCard { get; set; }
        public long InstallmentCount { get; set; }
    }
}
