﻿using System;

namespace Radix.Payment.Domain.Models.Stone.Request
{
    public partial class Order
    {
        public string OrderReference { get; set; }
    }
}
