﻿using System;

namespace Radix.Payment.Domain.Models.Stone.Response
{
    public partial class CreditCardTransactionResultCollection
    {
        public string AcquirerMessage { get; set; }
        public string AcquirerName { get; set; }
        public long AcquirerReturnCode { get; set; }
        public string AffiliationCode { get; set; }
        public long AmountInCents { get; set; }
        public long AuthorizationCode { get; set; }
        public long AuthorizedAmountInCents { get; set; }
        public long CapturedAmountInCents { get; set; }
        public DateTimeOffset CapturedDate { get; set; }
        public CreditCard CreditCard { get; set; }
        public string CreditCardOperation { get; set; }
        public string CreditCardTransactionStatus { get; set; }
        public object DueDate { get; set; }
        public long ExternalTime { get; set; }
        public string PaymentMethodName { get; set; }
        public object RefundedAmountInCents { get; set; }
        public bool Success { get; set; }
        public long TransactionIdentifier { get; set; }
        public string TransactionKey { get; set; }
        public string TransactionKeyToAcquirer { get; set; }
        public string TransactionReference { get; set; }
        public long UniqueSequentialNumber { get; set; }
        public object VoidedAmountInCents { get; set; }
    }
}
