﻿using System;

namespace Radix.Payment.Domain.Models.Stone.Response
{
    public partial class CreditCard
    {
        public string CreditCardBrand { get; set; }
        public string InstantBuyKey { get; set; }
        public bool IsExpiredCreditCard { get; set; }
        public string MaskedCreditCardNumber { get; set; }
    }
}
