﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Models.Stone.Response
{
    public partial class CreditCardResponse
    {
        public object ErrorReport { get; set; }
        public long InternalTime { get; set; }
        public string MerchantKey { get; set; }
        public string RequestKey { get; set; }
        public List<object> BoletoTransactionResultCollection { get; set; }
        public string BuyerKey { get; set; }
        public List<CreditCardTransactionResultCollection> CreditCardTransactionResultCollection { get; set; }
        public OrderResult OrderResult { get; set; }
    }
}
