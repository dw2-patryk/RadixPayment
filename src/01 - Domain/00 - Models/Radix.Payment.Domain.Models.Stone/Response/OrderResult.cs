﻿using System;

namespace Radix.Payment.Domain.Models.Stone.Response
{
    public partial class OrderResult
    {
        public DateTimeOffset CreateDate { get; set; }
        public string OrderKey { get; set; }
        public string OrderReference { get; set; }
    }
}
