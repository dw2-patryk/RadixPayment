﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Radix.Payment.Domain.Models.API
{
    [Table("Transacao")]
    public partial class Transacao
    {
        [Key]
        public long TransacaoId { get; set; } = 0;

        public string JsonTransacao { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? DataCadastro { get; set; }

        public Lojista lojista { get; set; } = new Lojista();
    }
}
