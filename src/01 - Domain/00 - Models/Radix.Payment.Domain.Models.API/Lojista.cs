﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Radix.Payment.Domain.Models.API
{
    [Table("Lojista")]
    public partial class Lojista
    {
        [Key]
        public long LojistaId { get; set; }

        [StringLength(50)]
        public string Nome { get; set; }

        [StringLength(20)]
        public string CNPJ { get; set; }

        [StringLength(20)]
        public string Usuario { get; set; }

        [StringLength(20)]
        public string Senha { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? DataCadastro { get; set; }

        public bool Ativo { get; set; }

        public List<LojistaAdquirente> adquirentes { get; set; } = new List<LojistaAdquirente>();
        public List<LojistaAntiFraude> antifraudes { get; set; } = new List<LojistaAntiFraude>();
        public List<Transacao> transacoes { get; set; } = new List<Transacao>();
    }
}
