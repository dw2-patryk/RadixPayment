﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Radix.Payment.Domain.Models.API
{
    [Table("LojistaAdquirente")]
    public partial class LojistaAdquirente
    {
        [Key]
        public long LojistaAdquirenteId { get; set; } = 0;

        public string MerchantKey { get; set; }

        public Lojista lojista { get; set; } = new Lojista();
        public Adquirente adquirente { get; set; } = new Adquirente();
    }
}
