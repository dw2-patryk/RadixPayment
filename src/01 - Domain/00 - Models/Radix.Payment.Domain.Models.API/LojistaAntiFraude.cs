﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Radix.Payment.Domain.Models.API
{
    [Table("LojistaAntiFraude")]
    public partial class LojistaAntiFraude
    {
        [Key]
        public long LojistaAntiFraudeId { get; set; } = 0;

        public string Login { get; set; }
        public string ApiKey { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public Lojista lojista { get; set; } = new Lojista();
        public AntiFraude antifraude { get; set; } = new AntiFraude();
    }
}
