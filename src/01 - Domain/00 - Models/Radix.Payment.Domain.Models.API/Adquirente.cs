﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Radix.Payment.Domain.Models.API
{
    [Table("Adquirente")]
    public partial class Adquirente
    {
        [Key]
        public long AdquirenteId { get; set; } = 0;

        [StringLength(50)]
        public string Nome { get; set; }

        [StringLength(100)]
        public string EndPoint { get; set; }

        public bool Ativo { get; set; } = false;
    }
}
