﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Radix.Payment.Domain.Models.API
{
    [Table("AntiFraude")]
    public partial class AntiFraude
    {
        [Key]
        public long AntiFraudeId { get; set; } = 0;

        [StringLength(50)]
        public string Nome { get; set; }

        [StringLength(100)]
        public string EndPoint { get; set; }

        public bool Ativo { get; set; } = false;
    }
}
