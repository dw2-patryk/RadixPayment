﻿using System;

namespace Radix.Payment.Domain.Models.ClearSale.Response
{
    public class Order
    {
        public string ID { get; set; }
        public string Status { get; set; }
        public string Score { get; set; }
    }
}
