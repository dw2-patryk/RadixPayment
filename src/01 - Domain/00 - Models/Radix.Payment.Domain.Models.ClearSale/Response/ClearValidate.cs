﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Models.ClearSale.Response
{
    public class ClearValidate
    {
        public List<Order> Orders { get; set; }
        public string TransactionID { get; set; }
    }
}
