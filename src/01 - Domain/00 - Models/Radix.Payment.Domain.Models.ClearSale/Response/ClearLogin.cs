﻿using System;

namespace Radix.Payment.Domain.Models.ClearSale.Response
{
    public class Token
    {
        public string Value { get; set; }
        public string ExpirationDate { get; set; }
    }

    public class ClearLogin
    {
        public Token Token { get; set; }
    }
}
