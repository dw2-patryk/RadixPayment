﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Models.ClearSale.Request
{
    public class Order
    {
        public string ID { get; set; }
        public DateTime Date { get; set; }
        public string Email { get; set; }
        public int TotalItems { get; set; }
        public int TotalOrder { get; set; }
        public int TotalShipping { get; set; }
        public string IP { get; set; }
        public string Currency { get; set; }
        public List<Payment> Payments { get; set; }
        public BillingData BillingData { get; set; }
        public ShippingData ShippingData { get; set; }
        public List<Item> Items { get; set; }
        public string SessionID { get; set; }
    }
}
