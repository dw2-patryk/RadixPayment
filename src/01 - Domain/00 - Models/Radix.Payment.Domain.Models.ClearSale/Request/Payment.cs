﻿using System;

namespace Radix.Payment.Domain.Models.ClearSale.Request
{
    public class Payment
    {
        public string Date { get; set; }
        public int Type { get; set; }
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public string CardExpirationDate { get; set; }
        public int Amount { get; set; }
        public int PaymentTypeID { get; set; }
        public int CardType { get; set; }
        public string CardBin { get; set; }
    }
}
