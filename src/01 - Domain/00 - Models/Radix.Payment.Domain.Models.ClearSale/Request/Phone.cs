﻿using System;

namespace Radix.Payment.Domain.Models.ClearSale.Request
{
    public class Phone
    {
        public int Type { get; set; }
        public int CountryCode { get; set; }
        public int AreaCode { get; set; }
        public string Number { get; set; }
    }
}
