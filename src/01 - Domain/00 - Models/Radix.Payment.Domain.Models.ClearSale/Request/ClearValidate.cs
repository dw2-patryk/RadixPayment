﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Models.ClearSale.Request
{
    public class ClearValidate
    {
        public string ApiKey { get; set; }
        public string LoginToken { get; set; }
        public List<Order> Orders { get; set; }
        public string AnalysisLocation { get; set; }
        public bool Reanalysis { get; set; }
    }
}
