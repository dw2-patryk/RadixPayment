﻿using System;

namespace Radix.Payment.Domain.Models.ClearSale.Request
{
    public class Address
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Comp { get; set; }
        public string ZipCode { get; set; }
        public string County { get; set; }
        public string Number { get; set; }
    }
}
