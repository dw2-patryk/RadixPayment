﻿using System;

namespace Radix.Payment.Domain.Models.ClearSale.Request
{
    public partial class ClearLogin
    {
        public Login Login { get; set; }
    }

    public partial class Login
    {
        public string Apikey { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
