﻿using System;

namespace Radix.Payment.Domain.Models.ClearSale.Request
{
    public class Item
    {
        public int ProductId { get; set; }
        public string ProductTitle { get; set; }
        public int Price { get; set; }
        public string Category { get; set; }
        public int Quantity { get; set; }
    }
}
