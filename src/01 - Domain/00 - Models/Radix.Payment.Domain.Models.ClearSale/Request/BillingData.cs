﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Models.ClearSale.Request
{
    public class BillingData
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }
        public List<Phone> Phones { get; set; }
    }
}
