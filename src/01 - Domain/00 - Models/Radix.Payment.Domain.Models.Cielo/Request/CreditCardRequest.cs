﻿using System;

namespace Radix.Payment.Domain.Models.Cielo.Request
{
    public partial class CreditCardRequest
    {
        public long MerchantOrderId { get; set; }
        public Customer Customer { get; set; }
        public Payment Payment { get; set; }
    }

}
