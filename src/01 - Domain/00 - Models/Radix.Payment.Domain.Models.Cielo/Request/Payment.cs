﻿using System;

namespace Radix.Payment.Domain.Models.Cielo.Request
{
    public partial class Payment
    {
        public string Type { get; set; }
        public long Amount { get; set; }
        public long Installments { get; set; }
        public string SoftDescriptor { get; set; }
        public CreditCard CreditCard { get; set; }
    }
}
