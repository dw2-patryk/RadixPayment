﻿using System;

namespace Radix.Payment.Domain.Models.Cielo.Request
{
    public partial class Customer
    {
        public string Name { get; set; }
    }
}
