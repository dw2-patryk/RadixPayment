﻿using System;

namespace Radix.Payment.Domain.Models.Cielo.Response
{
    public partial class CreditCardResponse
    {
        public long MerchantOrderId { get; set; }
        public Customer Customer { get; set; }
        public Payment Payment { get; set; }
    }
}
