﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.Models.Cielo.Response
{
    public partial class Payment
    {
        public long ServiceTaxAmount { get; set; }
        public long Installments { get; set; }
        public string Interest { get; set; }
        public bool Capture { get; set; }
        public bool Authenticate { get; set; }
        public CreditCard CreditCard { get; set; }
        public long ProofOfSale { get; set; }
        public string Tid { get; set; }
        public long AuthorizationCode { get; set; }
        public string PaymentId { get; set; }
        public string Type { get; set; }
        public long Amount { get; set; }
        public string Currency { get; set; }
        public string Country { get; set; }
        public List<object> ExtraDataCollection { get; set; }
        public long Status { get; set; }
        public long ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public List<Link> Links { get; set; }
    }
}
