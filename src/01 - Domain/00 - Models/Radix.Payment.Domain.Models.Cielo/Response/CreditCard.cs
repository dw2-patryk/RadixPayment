﻿using System;

namespace Radix.Payment.Domain.Models.Cielo.Response
{
    public partial class CreditCard
    {
        public string CardNumber { get; set; }
        public string Holder { get; set; }
        public string ExpirationDate { get; set; }
        public bool SaveCard { get; set; }
        public string Brand { get; set; }
    }
}
