﻿using System;

namespace Radix.Payment.Domain.Models.Cielo.Response
{
    public partial class Link
    {
        public string Method { get; set; }
        public string Rel { get; set; }
        public string Href { get; set; }
    }
}
