﻿using System;

namespace Radix.Payment.Domain.Models.Cielo.Response
{
    public partial class Customer
    {
        public string Name { get; set; }
    }
}
