﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.DTO
{
    public class OrderResponse
    {
        public string Status { get; set; }
    }
}
