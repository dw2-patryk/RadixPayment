﻿using System;
using System.Collections.Generic;

namespace Radix.Payment.Domain.DTO
{
    public class OrderRequest
    {
        public string OrderReference { get; set; } = "0";
        public string OrderType { get; set; } = "CreditCard";

        public string ID { get; set; }
        public string Data { get; set; }
        public string Email { get; set; }
        public int TotalItems { get; set; }
        public int TotalOrder { get; set; }
        public int TotalShipping { get; set; }
        public string IP { get; set; }

        public long Installments { get; set; }
        public string Currency { get; set; }
        public string CardBrand { get; set; }
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public string CardExpirationDate { get; set; } = "00/00";
        public int Amount { get; set; }
        public string CardBin { get; set; } = "0";

        public List<Product> products = new List<Product>();
        public Bill bill = new Bill();
        public Shipping shipping = new Shipping();

        public string AnalysisLocation { get; set; }
    }

    public class Product
    {
        public string ProductId { get; set; }
        public string ProductTitle { get; set; }
        public string Price { get; set; }
        public string Category { get; set; }
        public string Quantity { get; set; }
    }

    public class Bill
    {
        public Client client { get; set; } = new Client();
        public List<Phone> phones { get; set; } = new List<Phone>();
    }

    public class Shipping
    {
        public Client client { get; set; } = new Client();
        public List<Phone> phones { get; set; } = new List<Phone>();
    }

    public class Client
    {
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Comp { get; set; }
        public string ZipCode { get; set; }
        public string County { get; set; }
        public string Number { get; set; }
    }

    public class Phone
    {
        public string CountryCode { get; set; }
        public string AreaCode { get; set; }
        public string PhoneNumber { get; set; }
    }
}
