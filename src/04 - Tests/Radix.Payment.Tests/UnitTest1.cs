using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Radix.Payment.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void doPayment()
        {
            // foi solicitado somente um teste simples pelo cliente.
            var bus = new Domain.Business.Pay(0) { Testing = true };
            Assert.AreEqual(bus.doPayment(""), "OK");
        }
    }
}
