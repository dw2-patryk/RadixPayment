﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Radix.Payment.Infra.Data
{
    public class Lojista
    {
        public Domain.Models.API.Lojista GetItem(long _id)
        {
            var retorno = new Domain.Models.API.Lojista();
            using (var ctx = new RadixDb())
            {
                retorno = ctx.Lojistas
                             .Include(i => i.adquirentes)
                             .ThenInclude(ta => ta.adquirente)
                             .Include(i => i.antifraudes)
                             .ThenInclude(tf => tf.antifraude)
                             .Include(i => i.transacoes)
                             .Where(x => x.LojistaId == _id)
                             .FirstOrDefault();
            }

            return retorno;
        }

        public List<Domain.Models.API.Lojista> Listar()
        {
            List<Domain.Models.API.Lojista> retorno = null;
            using (var ctx = new RadixDb())
            {
                retorno = ctx.Lojistas
                             .Include(i => i.adquirentes)
                             .ThenInclude(ta => ta.adquirente)
                             .Include(i => i.antifraudes)
                             .ThenInclude(tf => tf.antifraude)
                             .ToList();
            }

            return retorno;
        }

        public bool Save(ref Domain.Models.API.Lojista _item)
        {
            var bRetorno = false;
            try
            {
                using (var ctx = new RadixDb())
                {
                    if (_item.LojistaId > 0) // update
                    {
                        var _id = _item.LojistaId;
                        var item = ctx.Lojistas.Where(x => x.LojistaId == _id).First();

                        ctx.Lojistas.Attach(item);
                        ctx.Entry(item).CurrentValues.SetValues(_item);
                    }
                    else
                    {
                        ctx.Lojistas.Add(_item);
                    }

                    ctx.SaveChanges();
                    bRetorno = true;
                }
            }
            catch (Exception ex)
            {
                bRetorno = false;
            }

            return bRetorno;
        }

        public bool Delete(Domain.Models.API.Lojista _item)
        {
            var bRetorno = false;
            using (var ctx = new RadixDb())
            {
                try
                {
                    if (_item.LojistaId > 0)
                    {
                        ctx.Lojistas.Attach(_item);
                        ctx.Lojistas.Remove(_item);
                        bRetorno = ctx.SaveChanges() > 0;
                    }
                }
                catch (Exception ex)
                {
                    bRetorno = false;
                }
            }

            return bRetorno;
        }
    }
}
