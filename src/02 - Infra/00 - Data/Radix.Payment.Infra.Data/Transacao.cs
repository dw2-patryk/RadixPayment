﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Radix.Payment.Infra.Data
{
    public class Transacao
    {
        public Domain.Models.API.Transacao GetItem(long _id)
        {
            var retorno = new Domain.Models.API.Transacao();
            using (var ctx = new RadixDb())
            {
                retorno = ctx.Transacoes
                             .Where(x => x.TransacaoId == _id)
                             .FirstOrDefault();
            }

            return retorno;
        }

        public List<Domain.Models.API.Transacao> Listar()
        {
            List<Domain.Models.API.Transacao> retorno = null;
            using (var ctx = new RadixDb())
            {
                retorno = ctx.Transacoes
                             .ToList();
            }

            return retorno;
        }

        public bool Save(Domain.Models.API.Transacao _item)
        {
            var bRetorno = false;
            try
            {
                using (var ctx = new RadixDb())
                {
                    if (_item.TransacaoId > 0) // update
                    {
                        var _id = _item.TransacaoId;
                        var item = ctx.Transacoes.Where(x => x.TransacaoId == _id).First();

                        ctx.Transacoes.Attach(item);
                        ctx.Entry(item).CurrentValues.SetValues(_item);
                    }
                    else
                    {
                        ctx.Transacoes.Add(_item);
                    }

                    ctx.SaveChanges();
                    bRetorno = true;
                }
            }
            catch (Exception ex)
            {
                bRetorno = false;
            }

            return bRetorno;
        }

        public bool Delete(Domain.Models.API.Transacao _item)
        {
            var bRetorno = false;
            using (var ctx = new RadixDb())
            {
                try
                {
                    if (_item.TransacaoId > 0)
                    {
                        ctx.Transacoes.Attach(_item);
                        ctx.Transacoes.Remove(_item);
                        bRetorno = ctx.SaveChanges() > 0;
                    }
                }
                catch (Exception ex)
                {
                    bRetorno = false;
                }
            }

            return bRetorno;
        }
    }
}
