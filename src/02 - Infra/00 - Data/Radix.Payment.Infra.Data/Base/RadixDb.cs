﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Radix.Payment.Infra.Data
{
    public partial class RadixDb : DbContext
    {
        public RadixDb()
        {
            // Database.SetInitializer<EFContext>(new DBInitializer());
            // this.Configuration.LazyLoadingEnabled = false; 
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var strConn = config.GetConnectionString("Local");
            strConn = strConn.Replace("%APIROOT%", Directory.GetCurrentDirectory());

            optionsBuilder.UseSqlServer(strConn);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Domain.Models.API.Lojista>().HasMany(o => o.adquirentes).WithOne(o => o.lojista);
            modelBuilder.Entity<Domain.Models.API.Lojista>().HasMany(o => o.antifraudes).WithOne(o => o.lojista);
            modelBuilder.Entity<Domain.Models.API.Lojista>().HasMany(o => o.transacoes).WithOne(o => o.lojista);

            modelBuilder.Entity<Domain.Models.API.LojistaAdquirente>().HasOne(o => o.adquirente);
            modelBuilder.Entity<Domain.Models.API.LojistaAntiFraude>().HasOne(o => o.antifraude);
        }

        public virtual DbSet<Domain.Models.API.Lojista> Lojistas { get; set; }
        public virtual DbSet<Domain.Models.API.Adquirente> Adquirentes { get; set; }
        public virtual DbSet<Domain.Models.API.AntiFraude> AntiFraudes { get; set; }
        public virtual DbSet<Domain.Models.API.LojistaAdquirente> LojistaAdquirentes { get; set; }
        public virtual DbSet<Domain.Models.API.LojistaAntiFraude> LojistaAntiFraudes { get; set; }
        public virtual DbSet<Domain.Models.API.Transacao> Transacoes { get; set; }
    }
}
